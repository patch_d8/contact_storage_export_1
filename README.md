# Export CSV via drupal/contact_storage_export

Corrige le problème d'export CSV du module `drupal/contact_storage_export` pour les PHP > 7.1.

## Installation via composer:

Editer le fichier composer.json et ajouter ou ajuster les lignes ci-dessous:

```
// ...
  "extra": {
    "patches": {
      "drupal/contact_storage_export": {
        "CSV Export": "https://gitlab.com/patch_d8/contact_storage_export_1/-/raw/master/fix.patch"
      }
    }
  }
// ...
```

Ensuite il suffit de lancer `composer install` pour que le patch soit automatiquement appliqué.
